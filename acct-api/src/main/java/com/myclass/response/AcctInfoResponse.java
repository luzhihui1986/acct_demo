package com.myclass.response;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class AcctInfoResponse implements Serializable {

    private String acctCode;

    private Integer custId;

    private String custName;

    private String acctName;

    private String accttypeCode;

    private BigDecimal totalBal;

    private BigDecimal availBal;

    private BigDecimal frozenBal;

    private BigDecimal notwithdrawBal;

    private String stat;

    private Date effDate;

    private Date expDate;

    private Date frozenDate;

    private BigDecimal balLimit;

    private String custType;

}