package com.myclass.service;

import com.myclass.response.AcctInfoResponse;

import java.util.List;

public interface AcctService {

    /**
     * 获取所有的账务信息
     * @return
     */
    List<AcctInfoResponse> queryAcctInfos();

}
