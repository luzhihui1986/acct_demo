package com.myclass.dao;

import com.myclass.AcctApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AcctApplication.class)
@WebAppConfiguration
public class AcctInfoMapperTest {

    @Autowired
    private AcctInfoMapper acctInfoMapper;

    @Test
    public void testQueryAcctInfos() {
        System.out.println(acctInfoMapper.selectAll());
    }

}
