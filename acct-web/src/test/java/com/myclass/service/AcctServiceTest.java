package com.myclass.service;

import com.myclass.AcctApplication;
import com.myclass.response.AcctInfoResponse;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AcctApplication.class)
@WebAppConfiguration
public class AcctServiceTest {

    @Autowired
    private AcctService acctService;

    @Test
    public void testQueryAcctInfos() {

        List<AcctInfoResponse> response = acctService.queryAcctInfos();

        System.out.println(response);
    }

}
