package com.myclass.entity;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Table(name = "t_acct_info")
public class AcctInfo {

    @Id
    private String acctCode;

    private Integer custId;

    private String custName;

    private String acctName;

    private String accttypeCode;

    private BigDecimal totalBal;

    private BigDecimal availBal;

    private BigDecimal frozenBal;

    private BigDecimal notwithdrawBal;

    private String stat;

    private Date effDate;

    private Date expDate;

    private Date frozenDate;

    private BigDecimal balLimit;

    private String custType;

}