package com.myclass.dao;

import com.myclass.entity.AcctInfo;
import tk.mybatis.mapper.common.Mapper;

public interface AcctInfoMapper  extends Mapper<AcctInfo> {

}