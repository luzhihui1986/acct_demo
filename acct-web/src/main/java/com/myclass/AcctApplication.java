package com.myclass;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ImportResource;
import tk.mybatis.spring.annotation.MapperScan;

@MapperScan(basePackages = {"com.myclass.dao"})
@SpringBootApplication(scanBasePackages = {"com.myclass"})
@ImportResource({"classpath:dubbo.xml"})
public class AcctApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(AcctApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(AcctApplication.class);
    }

}
