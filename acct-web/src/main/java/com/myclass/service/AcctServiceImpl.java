package com.myclass.service;

import com.myclass.dao.AcctInfoMapper;
import com.myclass.entity.AcctInfo;
import com.myclass.response.AcctInfoResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AcctServiceImpl implements AcctService{

    @Autowired
    private AcctInfoMapper acctInfoMapper;

    @Override
    public List<AcctInfoResponse> queryAcctInfos() {

        List<AcctInfo> acctInfos = acctInfoMapper.selectAll();

        List<AcctInfoResponse> acctInfoResponses = new ArrayList<AcctInfoResponse>();

        for (AcctInfo acctInfo : acctInfos) {

            AcctInfoResponse acctInfoResponse = new AcctInfoResponse();

            BeanUtils.copyProperties(acctInfo,acctInfoResponse);

            acctInfoResponses.add(acctInfoResponse);

        }

        return acctInfoResponses;
    }
}
